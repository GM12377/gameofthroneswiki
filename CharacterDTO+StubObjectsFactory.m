
#import "CharacterDTO+StubObjectsFactory.h"

@implementation CharacterDTO (StubObjectsFactory)
+ (instancetype)JohnSnow {
    CharacterDTO *product = [CharacterDTO characterWithId:@"2123" title:@"Jon Snow" ];
    product.thumbnailUrl = @"http://vignette3.wikia.nocookie.net/gameofthrones/images/4/49/Battle_of_the_Bastards_08.jpg/revision/latest/window-crop/width/200/x-offset/0/y-offset/0/window-width/2700/window-height/2700?cb=20160615184845";
    product.abstract = @"King Jon Snow is a major character in the first, second, third, fourth, fifth, sixth, and...";
    product.wikiUrl = @"http://gameofthrones.wikia.com/wiki/Jon_Snow";
    return product;
}
@end
