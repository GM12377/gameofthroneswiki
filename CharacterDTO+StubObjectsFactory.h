
#import "CharacterDTO.h"

/**
 CharacterDTO (StubObjectsFactory) contains convenience factories set for creating stub CharacterDTO objects.
 
 @interface CharacterDTO (StubObjectsFactory)
 @author Grzegorz Moscichowski
 */
@interface CharacterDTO (StubObjectsFactory)
+ (instancetype)JohnSnow;

@end
