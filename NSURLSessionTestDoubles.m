
#import "NSURLSessionTestDoubles.h"

@implementation NSURLSessionStub
- (NSURLSessionDataTask *)dataTaskWithURL:(NSURL *)url completionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error))completionHandler {
    __weak __typeof(self)weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        completionHandler(strongSelf.injectedData, nil, strongSelf.injectedError);
    });
    return nil;
}
@end

@implementation NSURLSessionAlwaysErrorStub
- (NSURLSessionDataTask *)dataTaskWithURL:(NSURL *)url completionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error))completionHandler {
    self.injectedError = [NSError errorWithDomain:@"test" code:1 userInfo:nil];
    return [super dataTaskWithURL:url completionHandler:completionHandler];
}
@end

@implementation NSURLSessionWithCharacterListStub
- (NSURLSessionDataTask *)dataTaskWithURL:(NSURL *)url completionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error))completionHandler {
    NSString *filename = [[NSBundle bundleForClass:[self class]] pathForResource:@"exampleTwoCharacters" ofType:@"txt"];
    self.injectedData = [NSData dataWithContentsOfFile:filename];
    return [super dataTaskWithURL:url completionHandler:completionHandler];
}
@end

@implementation NSURLSessionWithArticleStub
- (NSURLSessionDataTask *)dataTaskWithURL:(NSURL *)url completionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error))completionHandler {
    NSString *filename = [[NSBundle bundleForClass:[self class]] pathForResource:@"exampleArticle" ofType:@"txt"];
    self.injectedData = [NSData dataWithContentsOfFile:filename];
    return [super dataTaskWithURL:url completionHandler:completionHandler];
}
@end

@implementation NSURLSessionSpy
- (NSURLSessionDataTask *)dataTaskWithURL:(NSURL *)url completionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error))completionHandler {
    self.injectedURL = url;
    return [super dataTaskWithURL:url completionHandler:completionHandler];
}
@end