//
//  AppDelegate.h
//  GameOfThronesWiki
//
//  Created by Grzegorz Moscichowski on 04/07/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

