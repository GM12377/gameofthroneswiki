
#import <Foundation/Foundation.h>
#import "CharacterDTO.h"
#import "ArticleDTO.h"

/**
 GOTCharactersDataSource protocol for asyncrously obtaining CharacterDTO objects.
 
 @interface GOTCharactersDataSource
 @author Grzegorz Moscichowski
 */
@protocol GOTCharactersDataSource <NSObject>
@property BOOL filtered;

- (void) loadGOTCharactersWithCompletion:(void (^)(NSError *error))callbackBlock;
- (NSInteger) charactersCount;
- (CharacterDTO*) characterForIndex:(NSInteger)index;
- (void) loadExtendedCharacterDataForIndex:(NSInteger)index withCompletion:(void (^)(NSError *error))callbackBlock;

@end
