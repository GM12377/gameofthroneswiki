
#import "GOTCharactersNetworkDataSource.h"
#import "ArticleDTO.h"
#import "GOTArticleNetworkDataSource.h"

@interface GOTCharactersNetworkDataSource ()
@property NSArray<CharacterDTO*> *items;
@end

@implementation GOTCharactersNetworkDataSource
@synthesize filtered;
- (void) loadGOTCharactersWithCompletion:(void (^)(NSError *error))callbackBlock {
    NSURL *url = self.urlBuilder.mostViewedArts;
    [[self.urlSession dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *networkError) {
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSError *error = networkError;
            if (!error) {
                [self _processCharactersData:data error:error];
            }
            callbackBlock(error);
        });
    }] resume];
}

- (void) _processCharactersData:(NSData *)data error:(NSError*)error{
    NSArray *parsedResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    NSArray *items = [parsedResponse valueForKeyPath:@"items"];
    NSString *baseUrl = [parsedResponse valueForKeyPath:@"basepath"];
    NSMutableArray<CharacterDTO*> *product = [NSMutableArray new];
    for (NSDictionary *item in items) {
        CharacterDTO *dto = [CharacterDTO characterWithResponseDictionary:item baseUrl:baseUrl];
        if (dto) [product addObject:dto];
    }
    self.items = product;
}

- (NSInteger) charactersCount {
    return self.filteredItems.count;
}

- (NSArray<CharacterDTO*> *) filteredItems {
    if (!filtered) return self.items;
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"favourite = YES"];
    return [self.items filteredArrayUsingPredicate:pred];
}

- (CharacterDTO*) characterForIndex:(NSInteger)index {
    if (index < self.filteredItems.count) return self.filteredItems[index];
    return nil;
}

- (void) loadExtendedCharacterDataForIndex:(NSInteger)index withCompletion:(void (^)(NSError *))callbackBlock {
    CharacterDTO *character = [self characterForIndex:index];
    GOTArticleNetworkDataSource *articleDataSource = [GOTArticleNetworkDataSource new];
    [articleDataSource getArticleForCharacterId:character.identifier withCompletion:^(NSError *error, ArticleDTO *article) {
        character.abstractLong = article.articleText;
        callbackBlock(error);
    }];
}
@end
