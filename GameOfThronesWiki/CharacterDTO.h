
#import <Foundation/Foundation.h>

/**
 CharacterDTO is a data transfer object that represents single character data.
 
 @interface CharacterDTO
 @author Grzegorz Moscichowski
 */
@interface CharacterDTO : NSObject
@property NSString *identifier;
@property NSString *title;
@property NSString *thumbnailUrl;
@property NSString *wikiUrl;
@property NSString *abstract;
@property NSString *abstractLong;
@property BOOL favourite;

+ (instancetype) characterWithId:(NSString*)idetifier title:(NSString*)title;
+ (instancetype) characterWithResponseDictionary:(NSDictionary*)response;
+ (instancetype) characterWithResponseDictionary:(NSDictionary*)response baseUrl:(NSString*)baseUrl;

@end
