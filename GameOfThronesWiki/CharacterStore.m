
#import "CharacterStore.h"
#import "GOTCharactersNetworkDataSource.h"
#import "ImagesDataSource.h"

@interface CharacterStore ()
@property NSIndexPath *focusedCell;
@property NSMutableDictionary *viewDTOBuilders;
@property id<GOTCharactersDataSource> gotDataSource;
@property ImagesDataSource* imagesDataSource;

@end

@implementation CharacterStore
- (instancetype)init {
    self = [super init];
    if (self) {
        self.gotDataSource = [GOTCharactersNetworkDataSource new];
        self.imagesDataSource = [ImagesDataSource new];
        self.viewDTOBuilders = [NSMutableDictionary new];
    }
    return self;
}

- (void) tableView:(UITableView *)tableView loadCharactersListWithCompletion:(void (^)(NSError *))callbackBlock {
    [self.gotDataSource loadGOTCharactersWithCompletion:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^(void){
            if (!error) [tableView reloadData];
            callbackBlock(error);
        });
    }];
}

- (void) tableView:(UITableView *)tableView switchFocusToIndexPath:(NSIndexPath *)indexPath {
    if (indexPath != nil) {
        if (self.focusedCell) {
            [self _tableView:tableView setFocus:NO atIndexPath:self.focusedCell];
        }
        if ([indexPath isEqual:self.focusedCell]) {
            self.focusedCell = nil;
        } else {
            self.focusedCell = indexPath;
            [self _tableView:tableView setFocus:YES atIndexPath:indexPath];
        }
    }
}

- (void) _tableView:(UITableView *)tableView setFocus:(BOOL)focus atIndexPath:(NSIndexPath *)indexPath {
    CharacterViewDTOBuilder *dtoBuilder = [self characterDTOBuilderForindexPath:indexPath];
    dtoBuilder.extendedAbstract = focus;
    if (focus) {
        [dtoBuilder loadAbstractIfNeededWithCompletion:^(NSError *error) {
            [self _tableView:tableView updateRowsAtIndexPath:indexPath];
        }];
    }
    [self _tableView:tableView updateRowsAtIndexPath:indexPath];
}

- (void) _tableView:(UITableView *)tableView updateRowsAtIndexPath:(NSIndexPath*)indexPath {
    if  (self.focusedCell.row >= self.charactersCount) return;
    [tableView beginUpdates];
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [tableView endUpdates];
}

- (void) setFavourite:(BOOL)favourite onIndexPath:(NSIndexPath *)indexPath {
    CharacterViewDTOBuilder *viewBuilder = [self characterDTOBuilderForindexPath:indexPath];
    [viewBuilder setFavourite:favourite];
}

- (void) tableView:(UITableView *)tableView setFavouriteFilterMode:(BOOL)filter {
    self.gotDataSource.filtered = filter;
    [self.viewDTOBuilders removeAllObjects];
    [tableView reloadData];
}

- (CharacterViewDTOBuilder*) characterDTOBuilderForindexPath:(NSIndexPath *)indexPath {
    CharacterViewDTOBuilder *product = self.viewDTOBuilders[indexPath];
    if (!product) {
        CharacterDTO *character = [self.gotDataSource characterForIndex:indexPath.row];
        product = [CharacterViewDTOBuilder viewDTOBuilderWithCharacterDTO:character indexPath:indexPath];
        product.imagesDataSource = self.imagesDataSource;
        product.gotDataSource = self.gotDataSource;
        product.extendedAbstract = (self.focusedCell == indexPath);
        self.viewDTOBuilders[indexPath] = product;
    }
    return product;
}

- (NSInteger)charactersCount {
    return [self.gotDataSource charactersCount];
}

@end
