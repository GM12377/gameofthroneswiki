//
//  CharacterDetailsViewController.m
//  GameOfThronesWiki
//
//  Created by Grzegorz Moscichowski on 05/07/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

#import "CharacterDetailsViewController.h"
#import "GOTArticleDataSource.h"
#import "GOTArticleNetworkDataSource.h"

@interface CharacterDetailsViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *favouriteButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *thumbnailActivityIndicator;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *abstractActivityIndicator;

@end

@implementation CharacterDetailsViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewDTOBuilder.extendedAbstract = YES;
    [self _configure];
    [self.viewDTOBuilder loadAbstractIfNeededWithCompletion:^(NSError *error) {
        [self _configure];
    }];
    [self.viewDTOBuilder loadImageIfNeededWithCompletion:^(UIImage *image) {
        [self _configure];
    }];
}

- (void) _configure {
    CharacterViewDTO *dto = [self.viewDTOBuilder viewDTO];
    self.title = dto.title;
    self.imageView.image = dto.thumbnail;
    self.thumbnailActivityIndicator.hidden = !dto.thumbnailActivityIndicator;
    self.textView.text = dto.abstract;
    self.abstractActivityIndicator.hidden = !dto.abstractActivityIndicator;
    self.favouriteButton.selected = dto.favourite;
    [self.view setNeedsLayout];
}

- (IBAction)goToWikiPage:(id)sender {
    NSURL *url = [NSURL URLWithString:[self.viewDTOBuilder viewDTO].wikiUrl];
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)favouriteSwitch:(UIButton*)sender {
    sender.selected = !sender.selected;
    [self.viewDTOBuilder setFavourite:sender.selected];
}

@end
