//
//  ArticleDTOTests.m
//  GameOfThronesWiki
//
//  Created by Grzegorz Moscichowski on 05/07/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ArticleDTO.h"

@interface ArticleDTOTests : XCTestCase
@end

@implementation ArticleDTOTests
- (NSDictionary*)_exampleParsedArticle {
    NSString *filename = [[NSBundle bundleForClass:[self class]] pathForResource:@"exampleArticle" ofType:@"txt"];
    NSData *data = [NSData dataWithContentsOfFile:filename];
    NSError *error = nil;
    return [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
}

- (void)testInitWithExampleResponse {
    NSDictionary *response = [self _exampleParsedArticle];
    ArticleDTO *article = [ArticleDTO articleWithResponseDictionary:response];
    NSString *result = [article.paragraphs[0] substringToIndex:13];
    XCTAssertEqualObjects(result, @"King Jon Snow");
}
@end
