//
//  main.m
//  GameOfThronesWiki
//
//  Created by Grzegorz Moscichowski on 04/07/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
