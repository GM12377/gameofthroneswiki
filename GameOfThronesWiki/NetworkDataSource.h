
#import <Foundation/Foundation.h>
#import "WikiaUrlBuilder.h"

/**
 NetworkDataSource is a "abstract" network data source class with default initialization.
 
 @interface NetworkDataSource
 @author Grzegorz Moscichowski
 */
@interface NetworkDataSource : NSObject
@property WikiaUrlBuilder *urlBuilder;
@property NSURLSession *urlSession;

@end
