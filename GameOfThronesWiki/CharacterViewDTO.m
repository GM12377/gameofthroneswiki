
#import "CharacterViewDTO.h"

NSInteger const kShortenedAbstractLines = 2;

@interface CharacterViewDTO ()
@property (readwrite) NSInteger abstractMaxNumberOfLines;
@end

@implementation CharacterViewDTO
- (void) setFocus:(BOOL)focus {
    self.abstractMaxNumberOfLines = focus ? 0 : kShortenedAbstractLines;
}
@end
