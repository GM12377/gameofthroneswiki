
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 CharacterViewDTO dedicated dto for easy view data injection.
 
 @interface CharacterViewDTO
 @author Grzegorz Moscichowski
 */
@interface CharacterViewDTO : NSObject
@property NSString *title;
@property NSString *abstract;
@property (readonly) NSInteger abstractMaxNumberOfLines;
@property BOOL abstractActivityIndicator;
@property UIImage *thumbnail;
@property BOOL thumbnailActivityIndicator;
@property NSInteger tag;
@property UIColor *backgroundColor;
@property BOOL favourite;
@property NSString *wikiUrl;

- (void) setFocus:(BOOL)focus;

@end
