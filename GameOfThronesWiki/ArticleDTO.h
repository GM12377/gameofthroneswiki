
#import <Foundation/Foundation.h>

/**
 ArticleDTO is a data transfer object that represents wiki article data of single character.
 
 @interface ArticleDTO
 @author Grzegorz Moscichowski
 */
@interface ArticleDTO : NSObject
@property NSArray<NSString*> *paragraphs;
@property (readonly) NSString *articleText;

+ (instancetype)articleWithResponseDictionary:(NSDictionary*)response;

@end
