
#import <UIKit/UIKit.h>

/**
 CharacterListViewController is a viewController that displays single characters list.
 Tap on favorite icon at each of table view cells adds/removes character from favourites list.
 Tap on filter icon switches on/off filtered mode to show all or only favourite characters.
 
 @interface CharacterListViewController
 @author Grzegorz Moscichowski
 */
@interface CharacterListViewController : UIViewController

@end
