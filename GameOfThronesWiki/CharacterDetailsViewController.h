
#import <UIKit/UIKit.h>
#import "CharacterViewDTOBuilder.h"

/**
 CharacterDetailsViewController is a viewController that displays single character details data.
 Tap on favorite icon adds/removes character from favourites list.
 
 @interface CharacterDetailsViewController
 @author Grzegorz Moscichowski
 */
@interface CharacterDetailsViewController : UIViewController
@property CharacterViewDTOBuilder *viewDTOBuilder;

@end
