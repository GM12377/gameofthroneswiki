//
//  ImagesDataSource.h
//  GameOfThronesWiki
//
//  Created by Grzegorz Moscichowski on 06/07/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImagesDataSource : NSObject
- (UIImage*) getCachedImageWithUrlString:(NSString *)urlString;
- (void) getImageWithUrlString:(NSString *)urlString withCompletion:(void (^)(UIImage *image))callbackBlock;

@end
