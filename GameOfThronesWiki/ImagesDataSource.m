//
//  ImagesDataSource.m
//  GameOfThronesWiki
//
//  Created by Grzegorz Moscichowski on 06/07/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

#import "ImagesDataSource.h"

@interface ImagesDataSource ()
@property NSMutableDictionary *imagesCacheData;
@end

@implementation ImagesDataSource
- (UIImage*) getCachedImageWithUrlString:(NSString *)urlString {
    return self._imagesCache[urlString];
}

- (void) getImageWithUrlString:(NSString *)urlString withCompletion:(void (^)(UIImage *image))callbackBlock {
    UIImage *cachedImage = [self getCachedImageWithUrlString:urlString];
    if (cachedImage) {
        dispatch_async(dispatch_get_main_queue(), ^(void){
            callbackBlock(cachedImage);
        });
        return;
    }
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void){
        NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:urlString]];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            UIImage *downloadedImage = [[UIImage alloc]initWithData:data];
            self._imagesCache[urlString] = downloadedImage;
            callbackBlock(downloadedImage);
        });
    });
}

- (NSMutableDictionary*) _imagesCache {
    if (!self.imagesCacheData) {
        self.imagesCacheData = [NSMutableDictionary new];
    }
    return self.imagesCacheData;
}
@end
