//
//  WikiaUrlBuilderTests.m
//  GameOfThronesWiki
//
//  Created by Grzegorz Moscichowski on 05/07/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "WikiaUrlBuilder.h"

@interface WikiaUrlBuilderTests : XCTestCase
@property WikiaUrlBuilder *urlBuilder;
@end

@implementation WikiaUrlBuilderTests
- (void)setUp {
    [super setUp];
    self.urlBuilder = [WikiaUrlBuilder urlBuilderWithBaseUrlString:@"http://test.com"];
}

- (void)testMostViewedArtsUrl {
    NSURL *expected = [NSURL URLWithString:@"http://test.com/api/v1/Articles/Top?expand=1&category=Characters&limit=75"];
    XCTAssertEqualObjects(self.urlBuilder.mostViewedArts, expected);
}
@end
