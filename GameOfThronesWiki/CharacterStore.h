
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CharacterViewDTO.h"
#import "CharacterViewDTOBuilder.h"

/**
 CharacterStore manages data operations for character list view.
 
 @interface CharacterStore
 @author Grzegorz Moscichowski
 */
@interface CharacterStore : NSObject
- (void) tableView:(UITableView *)tableView loadCharactersListWithCompletion:(void (^)(NSError *error))callbackBlock;
- (void) tableView:(UITableView *)tableView switchFocusToIndexPath:(NSIndexPath*)indexPath;
- (void) setFavourite:(BOOL)favourite onIndexPath:(NSIndexPath*)indexPath;
- (void) tableView:(UITableView *)tableView setFavouriteFilterMode:(BOOL)filter;

- (CharacterViewDTOBuilder*) characterDTOBuilderForindexPath:(NSIndexPath *)indexPath;
- (NSInteger) charactersCount;

@end
