
#import <UIKit/UIKit.h>
#import "CharacterViewDTO.h"

/**
 CharacterTableViewCell view for display one character data in table view.
 
 @interface CharacterTableViewCell
 @author Grzegorz Moscichowski
 */
@interface CharacterTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *thumbnailActivityIndicator;

- (void) configureCellWith:(CharacterViewDTO*)viewDTO;

@end
