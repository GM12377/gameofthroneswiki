
#import "CharacterDTO.h"

@implementation CharacterDTO
+ (instancetype)characterWithId:(NSString *)idetifier title:(NSString *)title {
    return [[self alloc] initWithId:idetifier title:title];
}
- (instancetype)initWithId:(NSString *)idetifier title:(NSString *)title {
    self = [super init];
    if (self) {
        self.identifier = idetifier;
        self.title = title;
    }
    return self;
}

+ (instancetype)characterWithResponseDictionary:(NSDictionary*)response {
    return [[self alloc] initWithResponseDictionary:response];
}
- (instancetype)initWithResponseDictionary:(NSDictionary*)response {
    NSString *identifier = [self _stringFromResponse:response forKeypath:@"id"];
    NSString *title = [self _stringFromResponse:response forKeypath:@"title"];
    if (title.length==0 || identifier.length==0) return nil;
    self = [self initWithId:identifier title:title];
    if (self) {
        self.thumbnailUrl = [self _stringFromResponse:response forKeypath:@"thumbnail"];
        self.wikiUrl = [self _stringFromResponse:response forKeypath:@"url"];
        self.abstract = [self _stringFromResponse:response forKeypath:@"abstract"];
    }
    return self;
}

- (NSString*)_stringFromResponse:(NSDictionary*)response forKeypath:(NSString*)keypath {
    id object = [response valueForKeyPath:keypath];
    if ([object isKindOfClass:[NSString class]]) return object;
    if ([object isKindOfClass:[NSNumber class]]) return ((NSNumber*)object).stringValue;
    return @"";
}

+ (instancetype)characterWithResponseDictionary:(NSDictionary*)response baseUrl:(NSString*)baseUrl {
    CharacterDTO *product = [CharacterDTO characterWithResponseDictionary:response];
    if ((product.wikiUrl.length) > 0 && (baseUrl.length > 0)) {
        product.wikiUrl = [baseUrl stringByAppendingString:product.wikiUrl];
    }
    return product;
}

/// isEqual
- (BOOL)isEqualToCharacterDTO:(CharacterDTO *)character {
    if (!character) return NO;
    if (![self.identifier isEqualToString:character.identifier]) return NO;
    if (![self.title isEqualToString:character.title]) return NO;
    return YES;
}

- (BOOL)isEqual:(id)object {
    if (self == object) return YES;
    if (![object isKindOfClass:[CharacterDTO class]]) return NO;
    return [self isEqualToCharacterDTO:(CharacterDTO *)object];
}
@end
