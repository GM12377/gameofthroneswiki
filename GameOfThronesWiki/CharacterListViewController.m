//
//  CharacterListViewController.m
//  GameOfThronesWiki
//
//  Created by Grzegorz Moscichowski on 05/07/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

#import "CharacterListViewController.h"
#import "CharacterDetailsViewController.h"
#import "CharacterTableViewDataSource.h"
#import "CharacterStore.h"

@interface CharacterListViewController () <UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property BOOL filtered;
@property CharacterStore *characterStore;
@property id<UITableViewDataSource> tableViewDataSource;
@property CharacterViewDTOBuilder *detailViewData;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end

@implementation CharacterListViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Game Of Thrones";
    [self _filterSetup];
    [self _tableViewSetup];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    self.detailViewData.extendedAbstract = NO;
    if (self.filtered) {
        [self.characterStore tableView:self.tableView setFavouriteFilterMode:YES];
    } else {
        if (self.detailViewData) [self _updateRowsAtIndexPaths:@[self.detailViewData.indexPath]];
    }
    self.detailViewData = nil;
}

- (void)_tableViewSetup {
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 88;
    self.characterStore = [CharacterStore new];
    CharacterTableViewDataSource* dataSource = [CharacterTableViewDataSource new];
    dataSource.cellEventsTarget = self;
    dataSource.characterStore = self.characterStore;
    self.tableViewDataSource = dataSource;
    self.tableView.dataSource = dataSource;
    [self.activityIndicator startAnimating];
    [self.characterStore tableView:self.tableView loadCharactersListWithCompletion:^(NSError *error) {
        [self.activityIndicator stopAnimating];
        if (error) [self _showAlertWithTitle:@"Failed to obtain character list" error:error];
    }];
}

- (void) _showAlertWithTitle:(NSString*)title error:(NSError*)error {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:error.localizedDescription delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
    [alert show];
}

- (void)_filterSetup {
    self.filtered = NO;
    [self _setFilterIconForFilterState:self.filtered];
}

- (void)_setFilterIconForFilterState:(BOOL)enabled {
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithImage:[self _imageForFilterState:enabled] style:UIBarButtonItemStylePlain target:self action:@selector(filterStateChange:)];
    self.navigationItem.rightBarButtonItem = button;
}

- (UIImage*)_imageForFilterState:(BOOL)enabled {
    if (enabled) return [UIImage imageNamed:@"full_filter.png"];
    return [UIImage imageNamed:@"empty_filter.png"];
}

- (void)filterStateChange:(id)sender {
    self.filtered = !self.filtered;
    [self _setFilterIconForFilterState:self.filtered];
    [self.characterStore tableView:self.tableView setFavouriteFilterMode:self.filtered];
}

// <UITableViewDelegate>
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CharacterDetailsViewController *vc = [CharacterDetailsViewController new];
    CharacterViewDTOBuilder* datailViewData = [self.characterStore characterDTOBuilderForindexPath:indexPath];
    vc.viewDTOBuilder = datailViewData;
    self.detailViewData = datailViewData;
    [self.navigationController pushViewController:vc animated:YES];
}

// IBActions
- (IBAction)longPress:(UIGestureRecognizer*)gestureRecognizer {
    CGPoint location = [gestureRecognizer locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
   if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
       [self.characterStore tableView:self.tableView switchFocusToIndexPath:indexPath];
   }
}

- (void) _updateRowsAtIndexPaths:(NSArray<NSIndexPath*>*)indexPaths {
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
}

- (IBAction)favouriteButton:(UIButton*)sender {
    sender.selected = !sender.selected;
    [self.characterStore setFavourite:sender.selected onIndexPath:[NSIndexPath indexPathForRow:[sender tag] inSection:0]];
    if (self.filtered) [self.characterStore tableView:self.tableView setFavouriteFilterMode:YES];
}
@end
