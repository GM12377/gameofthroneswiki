
#import "ArticleDTO.h"

@implementation ArticleDTO
+ (instancetype)articleWithResponseDictionary:(NSDictionary*)response {
    return [[self alloc] initWithResponseDictionary:response];
}

- (instancetype)initWithResponseDictionary:(NSDictionary*)response {
    NSArray *sections = [response valueForKeyPath:@"sections"];
    NSArray *paragraphs;
    for (NSDictionary *section in sections) {
        if ([[section valueForKeyPath:@"level" ] isEqual:@1]) {
            paragraphs = [section valueForKeyPath:@"content.@unionOfObjects.text"];
        }
    }
    if (paragraphs.count==0) return nil;
    self = [self init];
    if (self) {
        self.paragraphs = paragraphs;
    }
    return self;
}

- (NSString*)articleText {
    return [self.paragraphs componentsJoinedByString:@"\n"];
}
@end
