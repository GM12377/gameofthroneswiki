
#import "CharacterTableViewDataSource.h"
#import "CharacterTableViewCell.h"
#import "CharacterViewDTO.h"

NSString *const kCellIdentifier = @"CellIdentifier";

@interface CharacterTableViewDataSource ()
@end

@implementation CharacterTableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.characterStore.charactersCount;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CharacterTableViewCell*)cellFromXibWithOwner:(id)owner {
    NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"CharacterTableViewCell" owner:owner options:nil];
    return views.firstObject;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CharacterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    if (!cell) {
        cell = [self cellFromXibWithOwner:self.cellEventsTarget];
    }
    CharacterViewDTOBuilder *viewDtoBuilder = [self.characterStore characterDTOBuilderForindexPath:indexPath];
    [cell configureCellWith:viewDtoBuilder.viewDTO];
    [viewDtoBuilder loadImageIfNeededWithCompletion:^(UIImage *image) {
        CharacterTableViewCell *cellToChange = (CharacterTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
        cellToChange.thumbnail.image = image;
        cellToChange.thumbnailActivityIndicator.hidden = YES;
    }];
    return cell;
}
@end
