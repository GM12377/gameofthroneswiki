//
//  GOTArticleNetworkDataSourceTests.m
//  GameOfThronesWiki
//
//  Created by Grzegorz Moscichowski on 06/07/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CharacterDTO+StubObjectsFactory.h"
#import "NSURLSessionTestDoubles.h"
#import "GOTArticleNetworkDataSource.h"

@interface GOTArticleNetworkDataSourceTests : XCTestCase
@property GOTArticleNetworkDataSource *dataDource;
@property XCTestExpectation *expectation;
@end

@implementation GOTArticleNetworkDataSourceTests
- (void)setUp {
    [super setUp];
    self.dataDource = [GOTArticleNetworkDataSource new];
    self.dataDource.urlBuilder = [WikiaUrlBuilder urlBuilderWithBaseUrlString:@"http://test.com"];
    self.expectation = [self expectationWithDescription:@"callback"];
}

- (void)_assertGettingArticleWithCallBlock:(void (^)(NSError *error, ArticleDTO *article))callbackBlock {
    CharacterDTO* character = [CharacterDTO JohnSnow];
    [self.dataDource getArticleForCharacterId:character.identifier withCompletion:^(NSError *error, ArticleDTO *article) {
        callbackBlock(error, article);
        [self.expectation fulfill];
    }];
    [self waitForExpectationsWithTimeout:1 handler:nil];
}

- (void)testGetDetailedCharacterUrlInjection {
    NSURLSessionSpy *sessionSpy = [NSURLSessionSpy new];
    self.dataDource.urlSession = sessionSpy;
    [self _assertGettingArticleWithCallBlock:^(NSError *error, ArticleDTO *article) {
        XCTAssertEqualObjects(sessionSpy.injectedURL.absoluteString, @"http://test.com/api/v1/Articles/AsSimpleJson?id=2123");
    }];
}

- (void)testGetDetailedCharacterJsonParsing {
    self.dataDource.urlSession = [NSURLSessionWithArticleStub new];
    [self _assertGettingArticleWithCallBlock:^(NSError *error, ArticleDTO *article) {
        XCTAssertEqual(article.articleText.length, 1027);
    }];
}
@end
