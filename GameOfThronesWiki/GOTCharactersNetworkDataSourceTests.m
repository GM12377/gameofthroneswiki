//
//  GOTCharactersNetworkDataSourceTests.m
//  GameOfThronesWiki
//
//  Created by Grzegorz Moscichowski on 04/07/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSURLSessionTestDoubles.h"
#import "CharacterDTO.h"
#import "CharacterDTO+StubObjectsFactory.h"
#import "GOTCharactersNetworkDataSource.h"

@interface GOTCharactersNetworkDataSourceTests : XCTestCase
@property GOTCharactersNetworkDataSource *dataDource;
@property XCTestExpectation *expectation;
@end

@implementation GOTCharactersNetworkDataSourceTests
- (void)setUp {
    [super setUp];
    self.dataDource = [GOTCharactersNetworkDataSource new];
    self.dataDource.urlBuilder = [WikiaUrlBuilder urlBuilderWithBaseUrlString:@"http://test.com"];
    self.expectation = [self expectationWithDescription:@"callback"];
}

- (void)_assertGettingCharactersWithCallBlock:(void (^)(NSError *error))callbackBlock {
    [self.dataDource loadGOTCharactersWithCompletion:^(NSError *error) {
        callbackBlock(error);
        [self.expectation fulfill];
    }];
    [self waitForExpectationsWithTimeout:1 handler:nil];
}

- (void)testFailedRequest {
    self.dataDource.urlSession = [NSURLSessionAlwaysErrorStub new];
    [self _assertGettingCharactersWithCallBlock:^(NSError *error) {
        XCTAssert(error);
    }];
}

- (void)testSuccessfullRequest {
    self.dataDource.urlSession = [NSURLSessionWithCharacterListStub new];
    [self _assertGettingCharactersWithCallBlock:^(NSError *error) {
        XCTAssertEqual(self.dataDource.charactersCount, 2);
    }];
}

- (void)testRequestUrlInjectedToSession {
    NSURLSessionSpy *sessionSpy = [NSURLSessionSpy new];
    self.dataDource.urlSession =  sessionSpy;
    [self _assertGettingCharactersWithCallBlock:^(NSError *error) {
        XCTAssertEqualObjects(sessionSpy.injectedURL, self.dataDource.urlBuilder.mostViewedArts);
    }];
}

- (void)testResponseJsonParsing {
    CharacterDTO* expectedCharacter = [CharacterDTO JohnSnow];
    self.dataDource.urlSession =  [NSURLSessionWithCharacterListStub new];
    [self _assertGettingCharactersWithCallBlock:^(NSError *error) {
        CharacterDTO* parsedObject = [self.dataDource characterForIndex:1];
        XCTAssertEqualObjects(parsedObject, expectedCharacter);
        XCTAssertEqualObjects(parsedObject.abstract, expectedCharacter.abstract);
        XCTAssertEqualObjects(parsedObject.wikiUrl, expectedCharacter.wikiUrl);
    }];
}
@end
