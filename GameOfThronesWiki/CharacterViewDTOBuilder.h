
#import <Foundation/Foundation.h>
#import "CharacterDTO.h"
#import "CharacterViewDTO.h"
#import "GOTCharactersNetworkDataSource.h"
#import "ImagesDataSource.h"

/**
 CharacterViewDTOBuilder prepares CharacterViewDTO for view layer. Supports async image & article data loading.
 
 @interface CharacterViewDTOBuilder
 @author Grzegorz Moscichowski
 */
@interface CharacterViewDTOBuilder : NSObject
@property id<GOTCharactersDataSource> gotDataSource;
@property ImagesDataSource* imagesDataSource;
@property BOOL extendedAbstract;
@property NSIndexPath *indexPath;

+ (instancetype) viewDTOBuilderWithCharacterDTO:(CharacterDTO*)characterDTO indexPath:(NSIndexPath*)indexPath;
- (CharacterViewDTO*) viewDTO;
- (void) loadImageIfNeededWithCompletion:(void (^)(UIImage *image))callbackBlock;
- (void) loadAbstractIfNeededWithCompletion:(void (^)(NSError *error))callbackBlock;
- (void) setFavourite:(BOOL)favourite;

@end
