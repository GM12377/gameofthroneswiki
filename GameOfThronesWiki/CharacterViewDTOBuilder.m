
#import "CharacterViewDTOBuilder.h"

@interface CharacterViewDTOBuilder ()
@property CharacterDTO *characterDTO;
@end

@implementation CharacterViewDTOBuilder
+ (instancetype) viewDTOBuilderWithCharacterDTO:(CharacterDTO *)characterDTO indexPath:(NSIndexPath *)indexPath {
    return [[self alloc] initWithCharacterDTO:characterDTO indexPath:indexPath];
}

- (instancetype) initWithCharacterDTO:(CharacterDTO *)characterDTO indexPath:(NSIndexPath *)indexPath {
    self = [super init];
    if (self) {
        self.characterDTO = characterDTO;
        self.indexPath = indexPath;
    }
    return self;
}

- (CharacterViewDTO*) viewDTO {
    CharacterViewDTO *product = [CharacterViewDTO new];
    product.tag = self.indexPath.row;
    product.backgroundColor = (self.indexPath.row % 2 == 0) ? [UIColor whiteColor] : [UIColor lightGrayColor];
    product.title = self.characterDTO.title;
    
    [product setFocus:self.extendedAbstract];
    product.abstract = [self _abstractString];
    product.abstractActivityIndicator = (![self _isExtendedAbstractLoaded]) && self.extendedAbstract;
    
    product.thumbnail = [self.imagesDataSource getCachedImageWithUrlString:self.characterDTO.thumbnailUrl];
    product.thumbnailActivityIndicator = (product.thumbnail == nil);
    
    product.favourite = self.characterDTO.favourite;
    product.wikiUrl = self.characterDTO.wikiUrl;
    return product;
}

- (NSString*) _abstractString {
    if ([self _isExtendedAbstractLoaded]) return self.characterDTO.abstractLong;
    return self.characterDTO.abstract;
}

- (BOOL) _isExtendedAbstractLoaded {
    return self.characterDTO.abstractLong.length > 0;
}

- (void) loadImageIfNeededWithCompletion:(void (^)(UIImage *))callbackBlock {
    UIImage *thumbnail = [self.imagesDataSource getCachedImageWithUrlString:self.characterDTO.thumbnailUrl];
    if (thumbnail) return;
    [self.imagesDataSource getImageWithUrlString:self.characterDTO.thumbnailUrl withCompletion:^(UIImage *image) {
        callbackBlock(image);
    }];
}

- (void) loadAbstractIfNeededWithCompletion:(void (^)(NSError *))callbackBlock { // TODO !!! if needed
    [self.gotDataSource loadExtendedCharacterDataForIndex:self.indexPath.row withCompletion:^(NSError *error) {
        self.characterDTO = [self.gotDataSource characterForIndex:self.indexPath.row];
        callbackBlock(error);
    }];
}

- (void) setFavourite:(BOOL)favourite {
    self.characterDTO.favourite = favourite;
}
@end
