//
//  CharacterDTOTests.m
//  GameOfThronesWiki
//
//  Created by Grzegorz Moscichowski on 05/07/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CharacterDTO.h"

@interface CharacterDTOTests : XCTestCase
@end

@implementation CharacterDTOTests
- (void)testParsingNilResponse {
    XCTAssertNil([CharacterDTO characterWithResponseDictionary:nil]);
}

- (void)testParsingEmptyResponse {
    XCTAssertNil([CharacterDTO characterWithResponseDictionary:@{}]);
}

- (void)testParsingPartialDataResponse1 {
    CharacterDTO *parsedResponse = [CharacterDTO characterWithResponseDictionary:@{@"title":@"A"}];
    XCTAssertNil(parsedResponse);
}

- (void)testParsingPartialDataResponse2 {
    CharacterDTO *parsedResponse = [CharacterDTO characterWithResponseDictionary:@{@"id":@1, @"title":@"A"}];
    CharacterDTO *expectedDTO = [CharacterDTO characterWithId:@"1" title:@"A"];
    XCTAssertEqualObjects(parsedResponse, expectedDTO);
}
@end
