
#import "WikiaUrlBuilder.h"

NSString *const kMostViewed = @"/api/v1/Articles/Top?expand=1&category=Characters&limit=75";
NSString *const kCharacterArticle = @"/api/v1/Articles/AsSimpleJson?id=";
NSString *const kDefaultBaseUrl = @"http://gameofthrones.wikia.com";

@implementation WikiaUrlBuilder
- (instancetype)init {
    return [self initWithBaseUrlString:kDefaultBaseUrl];
}

- (instancetype)initWithBaseUrlString:(NSString *)baseUrlString {
    self = [super init];
    if (self) {
        self.wikiaBaseUrlString = baseUrlString;
    }
    return self;
}

+ (instancetype) urlBuilderWithBaseUrlString:(NSString *)baseUrlString {
    return [[self alloc] initWithBaseUrlString:baseUrlString];
}

/// interface
- (NSURL*) mostViewedArts {
    NSString *urlString = [self.wikiaBaseUrlString stringByAppendingString:kMostViewed];
    return [NSURL URLWithString:urlString];
}

- (NSURL*) characterDetailsWithId:(NSString*)identifier {
    if (identifier.length < 1) return nil;
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@", self.wikiaBaseUrlString, kCharacterArticle, identifier];
    return [NSURL URLWithString:urlString];
}
@end
