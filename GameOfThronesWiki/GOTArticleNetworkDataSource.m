
#import "GOTArticleNetworkDataSource.h"
#import "GOTArticleDataSource.h"

@implementation GOTArticleNetworkDataSource
- (void) getArticleForCharacterId:(NSString*)identifier withCompletion:(void (^)(NSError *error, ArticleDTO *article))callbackBlock {
    NSURL *url = [self.urlBuilder characterDetailsWithId:identifier];
    [[self.urlSession dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *networkError) {
        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSError *error = networkError;
            ArticleDTO *article;
            if (!error) {
                article = [self _getArticleTextFromData:data error:error];
            }
            callbackBlock(error,article);
        });
    }] resume];
}

- (ArticleDTO*) _getArticleTextFromData:(NSData *)data error:(NSError*)error {
    NSDictionary *parsedResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    ArticleDTO *article = [ArticleDTO articleWithResponseDictionary:parsedResponse];
    return article;
}
@end
