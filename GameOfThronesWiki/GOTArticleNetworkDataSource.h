
#import <Foundation/Foundation.h>
#import "NetworkDataSource.h"
#import "GOTArticleDataSource.h"

/**
 GOTArticleNetworkDataSource is a network data source with GOTArticleDataSource protocol implemetation.
 
 @interface GOTArticleNetworkDataSource
 @author Grzegorz Moscichowski
 */
@interface GOTArticleNetworkDataSource : NetworkDataSource<GOTArticleDataSource>

@end
