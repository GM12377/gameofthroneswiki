
#import "CharacterTableViewCell.h"

@interface CharacterTableViewCell ()
@property (weak, nonatomic) IBOutlet UITextView *abstract;
@property (weak, nonatomic) IBOutlet UITextField *title;
@property (weak, nonatomic) IBOutlet UIView *textExclusionView;
@property (weak, nonatomic) IBOutlet UIButton *favouriteButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *abstractActivityIndicator;

@end

@implementation CharacterTableViewCell
- (void) configureCellWith:(CharacterViewDTO*)viewDTO {
    CGRect imageRect = self.textExclusionView.bounds;
    UIBezierPath * imgRect = [UIBezierPath bezierPathWithRect:imageRect];
    self.abstract.textContainer.exclusionPaths = @[imgRect];
    self.abstract.textContainer.lineBreakMode = NSLineBreakByTruncatingTail;
    self.abstractActivityIndicator.hidden = !viewDTO.abstractActivityIndicator;
    self.favouriteButton.tag = viewDTO.tag;
    self.favouriteButton.selected = viewDTO.favourite;
    self.backgroundColor = viewDTO.backgroundColor;
    self.thumbnail.image = viewDTO.thumbnail;
    self.thumbnailActivityIndicator.hidden = !viewDTO.thumbnailActivityIndicator;
    self.title.text = viewDTO.title;
    self.abstract.text = viewDTO.abstract;
    self.abstract.textContainer.maximumNumberOfLines = viewDTO.abstractMaxNumberOfLines;
}

@end
