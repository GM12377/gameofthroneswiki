
#import <Foundation/Foundation.h>

/**
 WikiaUrlBuilder simple url builder for all urls used in app with necessary parameter injection.
 
 @interface WikiaUrlBuilder
 @author Grzegorz Moscichowski
 */
@interface WikiaUrlBuilder : NSObject
@property NSString *wikiaBaseUrlString;

+ (instancetype) urlBuilderWithBaseUrlString:(NSString *)baseUrlString;

- (NSURL*) mostViewedArts;
- (NSURL*) characterDetailsWithId:(NSString*)identifier;

@end
