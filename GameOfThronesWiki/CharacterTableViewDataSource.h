
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CharacterStore.h"

/**
 CharacterTableViewDataSource prepares/dequeues cells for character list UITableView.
 Injects CharacterViewDTO into CharacterTableViewCell to let it configure itself.
 Sets CharacterTableViewCell image direct for better performace on older devices.
 
 @interface CharacterTableViewDataSource
 @author Grzegorz Moscichowski
 */
@interface CharacterTableViewDataSource : NSObject<UITableViewDataSource>
@property (weak,nonatomic) id cellEventsTarget;
@property CharacterStore *characterStore;

@end
