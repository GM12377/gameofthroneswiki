
#import <Foundation/Foundation.h>
#import "GOTCharactersDataSource.h"
#import "NetworkDataSource.h"

/**
 GOTCharactersNetworkDataSource is a network data source with GOTCharactersDataSource protocol implemetation.
 
 @interface GOTCharactersNetworkDataSource
 @author Grzegorz Moscichowski
 */
@interface GOTCharactersNetworkDataSource : NetworkDataSource<GOTCharactersDataSource>

@end
