
#import <Foundation/Foundation.h>
#import "ArticleDTO.h"
#import "CharacterDTO.h"

/**
 GOTArticleDataSource protocol for asyncrously obtaining ArticleDTO objects.
 
 @interface GOTArticleDataSource
 @author Grzegorz Moscichowski
 */
@protocol GOTArticleDataSource <NSObject>
- (void) getArticleForCharacterId:(NSString*)identifier withCompletion:(void (^)(NSError *error, ArticleDTO *article))callbackBlock;

@end
