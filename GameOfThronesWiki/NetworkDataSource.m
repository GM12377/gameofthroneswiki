
#import "NetworkDataSource.h"

@implementation NetworkDataSource
- (instancetype)init {
    self = [super init];
    if (self) {
        self.urlBuilder = [WikiaUrlBuilder new];
        self.urlSession = [NSURLSession sharedSession];
    }
    return self;
}

@end
