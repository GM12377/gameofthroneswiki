
#import <Foundation/Foundation.h>

/**
 NSURLSession stub superclass.
 
 @interface NSURLSession
 @author Grzegorz Moscichowski
 */
@interface NSURLSessionStub : NSURLSession
@property NSError *injectedError;
@property NSData *injectedData;
@end

/**
 NSURLSessionAlwaysErrorStub stub allways reporting communication errors.
 
 @interface NSURLSessionAlwaysErrorStub
 @author Grzegorz Moscichowski
 */
@interface NSURLSessionAlwaysErrorStub : NSURLSessionStub
@end

/**
 NSURLSessionWithCharacterListStub stub that allways responds with injected character data.
 
 @interface NSURLSessionWithCharacterListStub
 @author Grzegorz Moscichowski
 */
@interface NSURLSessionWithCharacterListStub : NSURLSessionStub
@end

/**
 NSURLSessionWithArticleStub stub that allways responds with injected article data.
 
 @interface NSURLSessionWithArticleStub
 @author Grzegorz Moscichowski
 */
@interface NSURLSessionWithArticleStub : NSURLSessionStub
@end

/**
 NSURLSessionSpy test double for checking injected url.
 
 @interface NSURLSessionSpy
 @author Grzegorz Moscichowski
 */
@interface NSURLSessionSpy : NSURLSessionWithCharacterListStub
@property NSURL* injectedURL;
@end